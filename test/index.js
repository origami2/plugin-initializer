var assert = require('assert');
var PluginInitializer = require('..');
var Plugin = require('origami-plugin');
var EventEmitter = require('events').EventEmitter;

function StubPlugin() {
}

StubPlugin.prototype.echo = function (what) {
  return Promise.resolve(what);
};

describe('PluginInitializer', function () {
  it('requires a plugin', function () {
    assert.throws(
      function () {
        new PluginInitializer();
      },
      /plugin is required/
    );
  });
  
  var plugin1;
  
  before(function () {
    plugin1 = new Plugin(StubPlugin);
  });
  
  it('returns a function', function () {
    var target = new PluginInitializer(plugin1);
    
    assert.equal('function', typeof(target));
  });
  
  it('requires a socket', function () {
    var target = new PluginInitializer(plugin1);
    
    assert
    .throws(
      function () {
        target();
      },
      /socket is required/
    );
  });
  
  it('requires a socket', function () {
    var target = new PluginInitializer(plugin1);
    
    assert
    .throws(
      function () {
        target();
      },
      /socket is required/
    );
  });
  
  it('requires a crane', function () {
    var target = new PluginInitializer(plugin1);
    var fakeSocket = new EventEmitter();
    
    assert
    .throws(
      function () {
        target(
          fakeSocket,
          'Plugin.StubPlugin'
        );
      },
      /crane is required/
    );
  });
  
  describe('#api event', function () {
    it('invokes method', function (done) {
      var target = new PluginInitializer({
        getName: function () { return 'StubPlugin'; },
        invokeMethod: function (stackToken, context, methodName, params) {
          try {
            assert.deepEqual(
              {
                fakeToken: true
              },
              stackToken
            );
            
            assert(context.emit);
            delete context.emit;
            assert.deepEqual(
              {
                someContext: 'someValue'
              },
              context
            );
            assert.equal('echo', methodName);
            assert.deepEqual(
              { what: 'this' },
              params
            );
            
            done();
          } catch (e) {
            done(e);
          }
          
          return new Promise(function () {
          });
        }
      });
      var fakeSocket = new EventEmitter();
      
      target(
        fakeSocket,
        'Plugin.StubPlugin',
        {}
      )
      .then(function () {
        fakeSocket
        .emit(
          'api',
          {
            fakeToken: true
          }, // stack token
          {
            someContext: 'someValue'
          }, // context
          'echo', // method name
          { 'what': 'this' }, // params
          function () {
          }
        );
      });
    });
    
    it('returns error on promise reject', function (done) {
      var target = new PluginInitializer({
        getName: function () { return 'StubPlugin'; },
        invokeMethod: function (stackToken, context, methodName, params) {
          return Promise.reject('some error');
        }
      });
      var fakeSocket = new EventEmitter();
      
      target(
        fakeSocket,
        'Plugin.StubPlugin',
        {}
      );
      
      fakeSocket
      .emit(
        'api',
        {
        }, // stack token
        {
        }, // context
        'echo', // method name
        { 'what': 'this' }, // params
        function (err) {
          try {
            assert.equal('some error', err);
            
            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });
    
    it('returns result on promise resolution', function (done) {
      var target = new PluginInitializer({
        getName: function () { return 'StubPlugin'; },
        invokeMethod: function (stackToken, context, methodName, params) {
          return Promise.resolve('my result');
        }
      });
      var fakeSocket = new EventEmitter();
      
      target(
        fakeSocket,
        'Plugin.StubPlugin',
        {}
      );
      
      fakeSocket
      .emit(
        'api',
        {
        }, // stack token
        {
        }, // context
        'echo', // method name
        { 'what': 'this' }, // params
        function (err, result) {
          try {
            assert(!err);
            assert.equal('my result', result);
            
            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });
  });
  
  describe('context client parameter', function () {
    it('requires a second socket for invoking dependencies', function (done) {
      var target = new PluginInitializer(
        {
          getName: function () { return 'StubPlugin'; },
          invokeMethod: function (stackToken, context, methodName, params) {
            return Promise.resolve('my result');
          }
        },
        {
          'dep1': 'Dep1'
        }
      );
      var fakeSocket = new EventEmitter();
      
      target(
        fakeSocket,
        'Plugin.StubPlugin',
        {
          createSocket: function (namespace, initializer) {
            try {
              assert.equal('', namespace);
              assert.equal('function', typeof(initializer));
              
              done();
            } catch (e) {
              done(e);
            }
          }
        }
      );
    });
  });
  
  describe('#get-methods event', function () {
    it('returns methods', function (done) {
      var target = new PluginInitializer({
        getName: function () { return 'StubPlugin'; },
        describeMethods: function () {
          return {
            'echo': [ 'what' ]
          };
        }
      });
      var fakeSocket = new EventEmitter();
      
      target(
        fakeSocket,
        'Plugin.StubPlugin',
        {}
      )
      .then(function () {
        fakeSocket
        .emit(
          'get-methods',
          function (err, methods) {
            try {
              assert(!err);
              assert.deepEqual(
                {
                  'echo': [ 'what' ]
                },
                methods
              );
              
              done();
            } catch (e) {
              done(e);
            }
          }
        );
      });
    });
  });
  
  describe('events', function () {
   it('subscribes to event if provided', function (done) {
      var target = new PluginInitializer(
        {
          getName: function () { return 'StubPlugin'; }
        }, 
        {},
        {
          'my-event': function () {}
        }
      );
      var fakeSocket = new EventEmitter();
      
      fakeSocket
        .on(
          'subscribe',
          function (eventName) {
            try {
              assert.equal('my-event', eventName);
              
              done();
            } catch (e) {
              done(e);
            }
          }
        );
      
      target(
        fakeSocket,
        'Plugin.StubPlugin',
        {}
      )
      .then(function () {
      });
    }); 
    
    it('passes event context', function (done) {
      var theToken = { theToken: true };
      var context = { user: 'me' };
      var target = new PluginInitializer(
        {
          getName: function () { return 'StubPlugin'; },
          createInstance: function (receivedContext, receivedToken) {
            try {
              assert.deepEqual(theToken, receivedToken);
              assert.deepEqual(context, receivedContext);
              
              done();
            } catch (e) {
              done(e);
            }
          }
        }, 
        {},
        {
          'my-event': function () {
          }
        }
      );
      var fakeSocket = new EventEmitter();
      
      target(
        fakeSocket,
        'Plugin.StubPlugin',
        {}
      )
      .then(function () {
        fakeSocket
        .emit(
          'publish',
          'my-event',
          { 
            stackToken: theToken,
            context: context,
            params: { 'hello': 'there' }
          }
        );
      });
    }); 
    
    it('runs function on event', function (done) {
      var target = new PluginInitializer(
        {
          getName: function () { return 'StubPlugin'; },
          createInstance: function () {
            return Promise.resolve({ isMyInstance: true });
          }
        }, 
        {},
        {
          'my-event': function (message) {
            try {
              assert.equal(true, this.isMyInstance);
              assert.deepEqual({'hello': 'there'}, message);
              
              done();
            } catch (e) {
              done(e);
            }
          }
        }
      );
      var fakeSocket = new EventEmitter();
      
      target(
        fakeSocket,
        'Plugin.StubPlugin',
        {}
      )
      .then(function () {
        fakeSocket
        .emit(
          'publish',
          'my-event',
          {
            context: {},
            params: { 'hello': 'there' }
          }
        );
      });
    }); 
  });
});