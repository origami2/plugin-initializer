var Client = require('origami-client');

function setupPlugin (plugin, socket, apis, dependenciesSocket, dependenciesMap, events) {
  socket
  .on(
    'get-methods',
    function (callback) {
      callback(null, plugin.describeMethods());
    }
  );
  
  socket
  .on(
    'api',
    function (stackToken, context, methodName, params, callback) {
      if (dependenciesMap && Object.keys(dependenciesMap).length > 0) {
        var client = new Client(dependenciesSocket, apis, stackToken);
         
        for (var localName in dependenciesMap) {
          context[localName] = client[dependenciesMap[localName]];
        }
      }
      
      context.emit = function (eventName, message) {
        socket.emit('event', stackToken, eventName, message);
      };
      
      plugin
      .invokeMethod(
        stackToken,
        context,
        methodName,
        params
      )
      .then(function (result) {
        callback(null, result);
      })
      .catch(function (err) {
        callback(err);
      });
    }
  );
  
  for (var eventName in events) {
    socket
    .emit(
      'subscribe',
      eventName
    );
  }
  
  socket
  .on(
    'publish',
    function (eventName, message) {
      if (eventName === 'stack:plugin-updated') {
        apis[message.name] = message.methods;
        
        return;  
      }
      
      var stackToken = message.stackToken;
      var context = message.context;
      var params = message.params;
      
      if (events[eventName]) {
        plugin
        .createInstance(context, stackToken)
        .then(function (instance) {
          events[eventName].call(instance, params);
        });
      }
    }
  );
}

function PluginInitializer(plugin, dependenciesMap, events) {
  // Plugin initializer
  
  if (!plugin) throw new Error('plugin is required');
  
  events = events || {};
  
  return function (socket, namespace, crane) {
    // Plugin initializer
  
    if (!socket) throw new Error('socket is required');
    if (!crane) throw new Error('crane is required');
    if (namespace !== 'Plugin.' + plugin.getName()) throw new Error('namespace must be equal to Plugin. + plugin name');
    
    return new Promise(function (resolve, reject) {
      if (dependenciesMap && Object.keys(dependenciesMap).length > 0) {
        crane
        .createSocket(
          '',
          function (dependenciesSocket, namespace, crane) {
            dependenciesSocket
            .emit('describe-apis', function (err, apis) {
              if (err) return reject();
              
              socket
              .emit(
                'subscribe',
                'stack:plugin-updated'
              );
              
              setupPlugin(
                plugin,
                socket,
                apis,
                dependenciesSocket,
                dependenciesMap,
                events
              );
              
              resolve(function () {
                // ready function
              });
            });
          }
        );
      } else {
        setupPlugin(plugin, socket, {}, null, dependenciesMap, events);
        
        return resolve(function () {
          // ready function
        });
      }
    });
  };
}

module.exports = PluginInitializer;